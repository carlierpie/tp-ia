# TP-IA

## Utilisation:

Vérifier que vous respecter les exigences notées dans le fichier requirements.txt

Si vous voulez utiliser vos propres données, modifier le fichier antivirus_dataset.csv dans le dossier data avec vos données.   
Sinon des données sont déjà présentent par défaut.

Pour lancer le programme il faut se rendre dans le dossier app qui est à la racine du répertoire.    
Puis taper dans un terminal la commande python loadData.py (ou python3 loadData.py suivant votre instalation).  
Les résultats seront affichés.