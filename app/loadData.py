import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import SVC
from sklearn import svm
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler, LabelEncoder
from sklearn.metrics import confusion_matrix
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
import pandas_profiling
import plotly.graph_objects as go
#import plotly.express as px

# Import Dataset

data = pd.read_csv("../data/antivirus_dataset.csv", sep="|")

# Split dataset in Training and Test sets

X = data.drop(['Name', 'md5', 'LoaderFlags', 'legitimate', 'MajorImageVersion','MajorLinkerVersion', 'MinorImageVersion', 'MinorLinkerVersion', 'AddressOfEntryPoint', 'BaseOfCode', 'BaseOfData', 'ExportNb', 'FileAlignment', 'ImageBase', 'ImportsNbOrdinal', 'LoadConfigurationSize', 'MajorOperatingSystemVersion', 'MinorOperatingSystemVersion', 'MinorSubsystemVersion', 'NumberOfRvaAndSizes', 'ResourcesMaxSize', 'ResourcesMeanSize', 'ResourcesMinSize', 'ResourcesNb', 'SectionAlignment', 'SectionMaxRawsize', 'SectionMaxVirtualsize', 'SectionsMeanRawsize', 'SectionsMeanVirtualsize', 'SectionsMinRawsize', 'SectionsMinVirtualsize', 'SizeOfCode', 'SizeOfHeaders', 'SizeOfHeapCommit', 'SizeOfImage', 'SizeOfInitializedData', 'SizeOfOptionalHeader', 'SizeOfStackCommit', 'SizeOfUninitializedData'], axis=1)
Y = data['legitimate']


X_train, X_test, Y_train, Y_test = train_test_split(X,Y, test_size=0.2)

# Standard Scaling
# A voir si c'est utile

sc = StandardScaler()
X_train = sc.fit_transform(X_train)
X_test = sc.transform(X_test)

######## Valeur

print ("Valeur")
a = "["
for i in Y_test[:20]:
	a += str(i)+" "
print (a+"]")

######## RandomForestClassifier

rfc = RandomForestClassifier()
rfc_fit = rfc.fit(X_train, Y_train)
pred_y = rfc.predict(X_test)

print ("Predict")
print (pred_y[:20])

# Erreur
print ("Erreur " )
print (rfc_fit.score(X_train, Y_train))

# confusion_matrix(y_test,pred_y)
tn, fp, fn, tp = confusion_matrix(Y_test,pred_y).ravel()
print ("Voici le nombre de true negative, false positive, false negative et true positive pour la random forest:")
print (tn, fp, fn, tp)

fig = go.Figure(data=go.Bar(x=["true negative","false positive","false negative", "true positive"],y=[tn, fp, fn, tp]))
fig.write_html('RandomForestClassifier.html', auto_open=True)


######## Decision Tree
tree=DecisionTreeClassifier()
tree_fit = tree.fit(X_train, Y_train)
pred_tree = tree.predict(X_test)

print ("Predict")
print (pred_tree[:20])

# Erreur
print ("Erreur " )
print (tree_fit.score(X_train, Y_train))

# confusion_matrix(y_test,pred_tree)
tn, fp, fn, tp = confusion_matrix(Y_test,pred_tree).ravel()
print ("Voici le nombre de true negative, false positive, false negative et true positive pour le decision tree:")
print (tn, fp, fn, tp)

fig = go.Figure(data=go.Bar(x=["true negative","false positive","false negative", "true positive"],y=[tn, fp, fn, tp]))
fig.write_html('DecisionTree.html', auto_open=True)

######## Regression logistique

logit = LogisticRegression()
logit_fit = logit.fit(X_train, Y_train)
pred_logit = logit.predict(X_test)

print (pred_logit[:20])

# Erreur
print ("Erreur " )
print (logit_fit.score(X_train, Y_train))

# confusion_matrix(y_test,pred_tree)
tn, fp, fn, tp = confusion_matrix(Y_test,pred_logit).ravel()
print ("Voici le nombre de true negative, false positive, false negative et true positive pour le decision tree:")
print (tn, fp, fn, tp)

fig = go.Figure(data=go.Bar(x=["true negative","false positive","false negative", "true positive"],y=[tn, fp, fn, tp]))
fig.write_html('RegressionLog.html', auto_open=True)